package main

import(
  "gitlab.com/Orion951/todo_app"
  "gitlab.com/Orion951/todo_app/pkg/handler"
  "gitlab.com/Orion951/todo_app/pkg/service"
  "gitlab.com/Orion951/todo_app/pkg/repository"
  "log"
  "github.com/spf13/viper"
)

func main(){
  if err := initConfig(); err != nil {
    log.Fatalf("error inirializing configs: %s", err.Error())
  }
  repos := repository.NewRepository()
  services := service.NewService(repos)
  handlers := handler.NewHandler(services)

  srv := new(todo.Server)
  if err := srv.Run(viper.GetString("8000"), handlers.InitRouters()); err != nil{
    log.Fatalf("error occured while running http server: %s", err.Error())
  }
}

func initConfig() error {
  viper.AddConfigPath("configs")
  viper.SetConfigName("config")
  return viper.ReadInConfig()
}
